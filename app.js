// function printGrid(arr){
//     for( let row = 0 ; row < 9 ; row++ ){
//       let str = ""
//       for( let col = 0 ; col < 9 ; col++ ){
//         str += `${arr[row][col].toString()} `
//       }
//       console.log(str)
//     }
// }


function findEmptyLocation(arr, loc){
  for( let row = 0 ; row < 9 ; row++ ){
    for( let col = 0 ; col < 9 ; col++ ){
      if( arr[row][col] == 0 ){
        loc[0] = row
        loc[1] = col
        return true
      }
    }
  }
  return false
}

function usedInRow(arr, row, num){
  for( let i = 0 ; i < 9 ; i++ ){
    if( arr[row][i] == num ){
      return true
    }
  }
  return false
}

function usedInCol(arr, col, num){
  for( let i = 0 ; i < 9 ; i++ ){
    if( arr[i][col] == num ){
      return true
    }
  }
  return false
}

function usedInBox(arr, rowOffset, colOffset, num){
  for( let row = 0 ; row < 3 ; row++ ){
    for( let col = 0 ; col < 3 ; col++ ){
      if( arr[ row + rowOffset ][ col + colOffset ] == num ){
        return true
      }
    }
  }
  return false
}

function checkLocationIsSafe(arr, row, col, num){
  return !usedInRow(arr,row,num) && !usedInCol(arr,col,num) && !usedInBox(arr, row - (row % 3), col - (col % 3), num)
}

function solve(arr){

  let loc = [0, 0]

  if( !findEmptyLocation(arr, loc) ){
    return true
  }

  let row = loc[0]
  let col = loc[1]

  for( let num = 1 ; num <= 9 ; num++ ){

    if( checkLocationIsSafe(arr,row,col,num) ){

      arr[row][col] = num

      if( solve(arr) ){
        return true
      }

      arr[row][col] = 0
    }
  }

  return false
}


const lineByLine = require('n-readlines');
const liner = new lineByLine('./sudoku.txt');
let line;
let count = 0

while ( ( line = liner.next() )  ) {
    
  line = line.toString('ascii')

  if( line.substring(0, 4) == "Grid" ){

    count++

    let grid = []

    for( let i = 0 ; i < 9 ; i++ ){

      let row = liner.next().toString('ascii')
      let arr = []

      for( let j = 0 ; j < 9 ; j++ ){
        arr[j] = parseInt( row[j] )
      }

      grid.push(arr)

    }

    if( solve(grid) ){
      //printGrid(grid)
      let sum = grid[0][0] + grid[0][1] + grid[0][2]
      console.log(`Grid ${count} answer: ${sum}`)
    }

  }

}

